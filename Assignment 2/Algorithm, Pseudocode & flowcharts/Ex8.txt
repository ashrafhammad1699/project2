////////
Algorithm

Step: BEGIN.
Step: PRINT ENTER YOUR CHOICE.
Step: ENTER YOUR CHOICE.
Step: ENTER TWO OPERANDS FOR OPERATION.
Step: USER WILL ENTER +,-,*,/ .
Step: SWITCH(OPERATOR)
Step: DO THE OPERATION.
Step: PRINT THE RESULT.





//////////
Pseudocode


1.		Begin 
2.		Print "Enter your choice" 
3.		Enter your choice 
4.		Enter two operands 
5.		If the user enters + or - or * or / then follow the below steps 
		else flow goes to default case & exit the program 
6.		Switch(operator) 
    a.	case +: 
		i.		Print ‘Addition’. 
		ii.		Print result of summation. 
		iii.		break 
    b.	case -: 
		i.		Print ‘Subtraction’. 
		ii.		Print result of substraction. 
		iii.		break 
    c.	case *: 
		i.		Print ‘Multiplication’. 
		ii.		Print result of multiplication. 
		iii.		break 
    d.	case /: 
		i.		Print ‘Division’. 
		ii.		Print result of division. 
		iii.		break 
	e.	default: 
		i.	Print ‘Invalid Option’. 