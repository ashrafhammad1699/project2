/////////
Algorithm


Step 1. START.
Step 2. INTEGER AREA,RADIUS.
Step 3. PRINT "ENTER THE RADIUS OF CIRCLE - "
Step 4. AREA=3.14*RADIUS*RADIUS.
Step 5. PRINT "AREA OF CIRCLE = "
Step 6. PRINT AREA.
Step 7. EXIT.



///////////
Pseudocode

 
BEGIN
	NUMBER r, area 
	INPUT r 
	area=3.14*r*r 
        OUTPUT area 
END